using System;
using System.Collections.Generic;
using System.Linq;

class D {
    static ulong Hash(int a1, int a2) {
        return (ulong)a1 * 0x9ddfea08eb382d69 + (ulong)a2;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var h = int.Parse(line[0]);
        var w = int.Parse(line[1]);
        var n = int.Parse(line[2]);
        var blocks = new Dictionary<ulong, int>();
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var x1 = int.Parse(line[0]) - 1;
            var x2 = int.Parse(line[1]) - 1;
            for (int j1 = x1 - 2; j1 <= x1; ++j1)
                if (0 <= j1 && j1 + 2 < h)
                    for (int j2 = x2 - 2; j2 <= x2; ++j2)
                        if (0 <= j2 && j2 + 2 < w) {
                            var hash = Hash(j1, j2);
                            var v = 0;
                            blocks.TryGetValue(hash, out v);
                            blocks[hash] = v + 1;
                        }
        }
        var answer = new long[10];
        foreach (var i in blocks) ++answer[i.Value];
        answer[0] = (long)(h - 2) * (w - 2) - answer.Sum();
        for (int i = 0; i < answer.Length; ++i) Console.WriteLine(answer[i]);
    }
}
