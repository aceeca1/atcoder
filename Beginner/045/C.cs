using System;

class C {
    public static void Main() {
        var s = Console.ReadLine();
        var a = new long[s.Length + 1];
        var b = new long[s.Length + 1];
        a[s.Length] = 0L;
        b[s.Length] = 1L;
        for (int i = s.Length - 1; i >= 0; --i) {
            var v = 0L;
            for (int j = i + 1; j <= s.Length; ++j) {
                v = v * 10L + (s[j - 1] - '0');
                a[i] += a[j] + v * b[j];
                b[i] += b[j];
            }
        }
        Console.WriteLine(a[0]);
    }
}
