using System;
using System.Collections.Generic;

class A {
    static HashSet<char> vowel = new HashSet<char>("aeiou");

    public static void Main() {
        var c = Console.ReadLine()[0];
        Console.WriteLine(vowel.Contains(c) ? "vowel" : "consonant");
    }
}
