using System;
using System.Text.RegularExpressions;

class C {
    static Regex Regex1 = new Regex("^(dream|dreamer|erase|eraser)*$");

    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Regex1.IsMatch(s) ? "YES" : "NO");
    }
}
