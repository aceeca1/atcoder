using System;
using System.Collections.Generic;
using System.Linq;

class D {
    static ulong Hash(int a1, int a2) {
        return (ulong)a1 * 0x9ddfea08eb382d69 + (ulong)a2;
    }

    class Graph {
        List<int>[] Out;
        public int[] C;

        public Graph(int n) {
            Out = new List<int>[n];
            for (int i = 0; i < n; ++i) Out[i] = new List<int>();
        }

        public void AddEdge(int s, int t) {
            Out[s].Add(t);
            Out[t].Add(s);
        }

        public void MakeConnection() {
            C = new int[Out.Length];
            var label = 1;
            for (int i = 0; i < Out.Length; ++i)
                if (C[i] == 0) Assign(i, label++);
            Out = null;
        }

        void Assign(int no, int label) {
            C[no] = label;
            foreach (var i in Out[no])
                if (C[i] == 0) Assign(i, label);
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        var l = int.Parse(line[2]);
        var graph1 = new Graph(n);
        for (int i = 0; i < k; ++i) {
            line = Console.ReadLine().Split();
            var s = int.Parse(line[0]) - 1;
            var t = int.Parse(line[1]) - 1;
            graph1.AddEdge(s, t);
        }
        graph1.MakeConnection();
        var graph2 = new Graph(n);
        for (int i = 0; i < l; ++i) {
            line = Console.ReadLine().Split();
            var s = int.Parse(line[0]) - 1;
            var t = int.Parse(line[1]) - 1;
            graph2.AddEdge(s, t);
        }
        graph2.MakeConnection();
        var hash = new ulong[n];
        var d = new Dictionary<ulong, int>();
        for (int i = 0; i < n; ++i) {
            hash[i] = Hash(graph1.C[i], graph2.C[i]);
            var v = 0;
            d.TryGetValue(hash[i], out v);
            d[hash[i]] = v + 1;
        }
        Console.WriteLine(string.Join(" ", hash.Select(k1 => d[k1])));
    }
}
