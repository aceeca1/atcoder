using System;
using System.Collections.Generic;
using System.Linq;

class D {
    static IEnumerable<char> Optimized() {
        while (true) {
            yield return 'g';
            yield return 'p';
        }
    }

    static int Value(char c) => c == 'p' ? 1 : 0;

    public static void Main() {
        var s = Console.ReadLine();
        var answer1 = s.Sum(Value);
        var answer2 = Optimized().Take(s.Length).Sum(Value);
        Console.WriteLine(answer2 - answer1);
    }
}
