using System;

class C {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        long a1 = 1L, a2 = 1L;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var b1 = int.Parse(line[0]);
            var b2 = int.Parse(line[1]);
            var k = Math.Max((a1 + b1 - 1) / b1, (a2 + b2 - 1) / b2);
            a1 = k * b1;
            a2 = k * b2;
        }
        Console.WriteLine(a1 + a2);
    }
}
