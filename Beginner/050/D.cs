using System;
using System.Collections.Generic;

class D {
    static Dictionary<long, long> M = new Dictionary<long, long>{
        {0L, 1L}, {1L, 2L}
    };

    static long F(long n) {
        var answer = 0L;
        if (M.TryGetValue(n, out answer)) return answer;
        return M[n] = (F(n >> 1) + F(n - 1 >> 1) + F(n - 2 >> 1)) % 1000000007;
    }

    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        Console.WriteLine(F(n));
    }
}
