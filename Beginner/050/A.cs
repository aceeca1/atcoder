using System;

class A {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[2]);
        switch (line[1][0]) {
            case '+': Console.WriteLine(a + b); break;
            case '-': Console.WriteLine(a - b); break;
        }
    }
}
