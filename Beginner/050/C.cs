using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

class C {
    static IEnumerable<int> Even() {
        int k = 1;
        while (true) {
            yield return k;
            yield return k;
            k += 2;
        }
    }

    static IEnumerable<int> Odd() {
        yield return 0;
        int k = 2;
        while (true) {
            yield return k;
            yield return k;
            k += 2;
        }
    }

    static bool Valid(int[] a) {
        Array.Sort(a);
        var trueSeq = (a.Length & 1) == 0 ? Even() : Odd();
        return trueSeq.Take(a.Length).SequenceEqual(a);
    }

    public static void Main() {
        Console.ReadLine();
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        if (Valid(a))
            Console.WriteLine(BigInteger.ModPow(2, a.Length >> 1, 1000000007));
        else Console.WriteLine(0);
    }
}
