using System;

class B {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = int.Parse(line[0]);
        var s = int.Parse(line[1]);
        var a = new int[4, s + 2];
        for (int i = 0; i <= s; ++i) a[0, i + 1] = 1;
        for (int i = 1; i <= 3; ++i)
            for (int j = 0; j <= s; ++j) {
                var a1 = a[i, j] + a[i - 1, j + 1];
                var a2 = k <= j ? a[i - 1, j - k] : 0;
                a[i, j + 1] = a1 - a2;
            }
        Console.WriteLine(a[3, s + 1] - a[3, s]);
    }
}
