using System;
using System.Collections.Generic;

class C {
    static IEnumerable<char> Go(int sx, int sy, int tx, int ty) {
        for (int i = sx; i < tx; ++i) yield return 'R';
        for (int i = sy; i < ty; ++i) yield return 'U';
        for (int i = tx; i > sx; --i) yield return 'L';
        for (int i = ty; i > sy; --i) yield return 'D';
        yield return 'D';
        for (int i = sx; i <= tx; ++i) yield return 'R';
        for (int i = sy; i <= ty; ++i) yield return 'U';
        yield return 'L';
        yield return 'U';
        for (int i = tx; i >= sx; --i) yield return 'L';
        for (int i = ty; i >= sy; --i) yield return 'D';
        yield return 'R';
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var sx = int.Parse(line[0]);
        var sy = int.Parse(line[1]);
        var tx = int.Parse(line[2]);
        var ty = int.Parse(line[3]);
        Console.WriteLine(string.Concat(Go(sx, sy, tx, ty)));
    }
}
