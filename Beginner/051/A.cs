using System;

class A {
    public static void Main() {
        var s = Console.ReadLine().Split(',');
        Console.WriteLine(string.Join(" ", s));
    }
}
