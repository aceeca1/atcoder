using System;

class D {
    static long F(long b, long n) {
        var answer = 0L;
        while (n != 0L) {
            answer += n % b;
            n /= b;
        }
        return answer;
    }

    static long Solve(long n, long s) {
        if (n == s) return n + 1L;
        var sqrtN = (long)Math.Round(Math.Sqrt(n));
        var sqrtNLower = sqrtN;
        if (n < sqrtNLower * sqrtNLower) --sqrtNLower;
        var sqrtNUpper = sqrtN;
        if (sqrtNUpper * sqrtNUpper < n) ++sqrtNUpper;
        for (long b = 2L; b < sqrtNUpper; ++b)
            if (F(b, n) == s) return b;
        for (long s1 = sqrtNLower; s1 != 0L; --s1) {
            var s2 = s - s1;
            var b = (n - s2) / s1;
            if (b >= 2L && F(b, n) == s) return b;
        }
        return -1L;
    }

    public static void Main() {
        var n = long.Parse(Console.ReadLine());
        var s = long.Parse(Console.ReadLine());
        Console.WriteLine(Solve(n, s));
    }
}
