using System;
using System.Linq;

class C {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = int.Parse(line[1]);
        var x = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var sum = n * a;
        var b = new long[sum + 1, n + 1];
        b[0, 0] = 1;
        foreach (var i in x)
            for (int j = sum; j >= i; --j)
                for (int k = 1; k <= n; ++k) b[j, k] += b[j - i, k - 1];
        var answer = Enumerable.Range(1, n).Sum(size => b[size * a, size]);
        Console.WriteLine(answer);
    }
}
