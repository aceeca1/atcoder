using System;
using System.Linq;

class C {
    public static void Main() {
        Console.ReadLine();
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var average = (int)Math.Round(a.Average());
        Console.WriteLine(a.Sum(k => (k - average) * (k - average)));
    }
}
