using System;

class D {
    static string Solve(string s) {
        for (int i = 1; i < s.Length; ++i)
            if (s[i] == s[i - 1]) return $"{i} {i + 1}";
        for (int i = 2; i < s.Length; ++i)
            if (s[i] == s[i - 2]) return $"{i - 1} {i + 1}";
        return "-1 -1";
    }

    public static void Main() {
        var s = Console.ReadLine();
        Console.WriteLine(Solve(s));
    }
}
