using System;

class A {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var k = int.Parse(Console.ReadLine());
        var x = int.Parse(Console.ReadLine());
        var y = int.Parse(Console.ReadLine());
        Console.WriteLine(n < k ? n * x : k * x + (n - k) * y);
    }
}
