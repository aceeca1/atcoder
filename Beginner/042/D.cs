using System;

// Snippet: FastPower
abstract class FastPower<T> {
    protected abstract T One();
    protected abstract T Times(T t1, T t2);

    public T Get(T n, long exp) {
        var answer = One();
        for (; exp != 0; exp >>= 1) {
            if ((exp & 1) != 0) answer = Times(answer, n);
            n = Times(n, n);
        }
        return answer;
    }
}

// Snippet: FastPowerModulo
class FastPowerModulo: FastPower<long> {
    public long M;
    protected override long One() => 1L;
    protected override long Times(long t1, long t2) => t1 * t2 % M;
}

// Snippet: ChooseModuloBigPrime
class ChooseModuloBigPrime {
    public static long Get(long n, long k, long modulo) {
        var fastPower = new FastPowerModulo{M = modulo};
        long answer1 = 1L, answer2 = 1L;
        for (long i = 1; i <= k; ++i) {
            answer1 = answer1 * (n + 1 - i) % modulo;
            answer2 = answer2 * i % modulo;
        }
        return answer1 * fastPower.Get(answer2, modulo - 2) % modulo;
    }

    public static long[] GetMultiple(long n1, long n2, long k, long modulo) {
        if (n2 < n1) {
            var answer2 = GetMultiple(n2, n1, k, modulo);
            Array.Reverse(answer2);
            return answer2;
        }
        var fastPower = new FastPowerModulo{M = modulo};
        var answer = new long[n2 - n1 + 1];
        answer[0] = Get(n1, k, modulo);
        for (int i = 1; i < answer.Length; ++i) {
            var answer1 = answer[i - 1] * (n1 + i) % modulo;
            var answer2 = answer1 * fastPower.Get(n1 + i - k, modulo - 2);
            answer[i] = answer2 % modulo;
        }
        return answer;
    }
}

class D {
    const long M = 1000000007;

    public static void Main() {
        var line = Console.ReadLine().Split();
        var h = long.Parse(line[0]);
        var w = long.Parse(line[1]);
        var a = long.Parse(line[2]);
        var b = long.Parse(line[3]);
        var c1 = ChooseModuloBigPrime.GetMultiple(
            h - a - 2 + w, h - a - 1 + b, h - a - 1, M);
        var c2 = ChooseModuloBigPrime.GetMultiple(
            a - 1, a - 2 + w - b, a - 1, M);
        var answer = 0L;
        for (int i = 0; i < c1.Length; ++i)
            answer = (answer + c1[i] * c2[i] % M) % M;
        Console.WriteLine(answer);
    }
}
