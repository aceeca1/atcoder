using System;
using System.Linq;

class A {
    static int[] Haiku = {5, 5, 7};

    public static void Main() {
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        Array.Sort(a);
        Console.WriteLine(Haiku.SequenceEqual(a) ? "YES" : "NO");
    }
}
