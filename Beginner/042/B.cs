using System;

class B {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var a = new string[n];
        for (int i = 0; i < n; ++i) a[i] = Console.ReadLine();
        Array.Sort(a);
        Console.WriteLine(string.Concat(a));
    }
}
