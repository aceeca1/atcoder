using System;
using System.Collections.Generic;
using System.Linq;

class C {
    static bool OK(HashSet<int> banned, int n) =>
        n.ToString().All(k => !banned.Contains(k - '0'));

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var d = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var banned = new HashSet<int>(d);
        while (!OK(banned, n)) ++n;
        Console.WriteLine(n);
    }
}
