using System;

class A {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var d = int.Parse(line[3]);
        Console.WriteLine(Math.Max(a * b, c * d));
    }
}
