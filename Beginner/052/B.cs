using System;

class B {
    public static void Main() {
        Console.ReadLine();
        var x = 0;
        var answer = 0;
        foreach (var i in Console.ReadLine()) {
            switch (i) {
                case 'I': ++x; break;
                case 'D': --x; break;
            }
            if (answer < x) answer = x;
        }
        Console.WriteLine(answer);
    }
}
