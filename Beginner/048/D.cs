using System;

class D {
    public static void Main() {
        var s = Console.ReadLine();
        var start = s.Length;
        var end = s[0] == s[s.Length - 1] ? 1 : 0;
        Console.WriteLine(((start - end) & 1) != 0 ? "First" : "Second");
    }
}
