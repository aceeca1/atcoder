using System;

class C {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = int.Parse(line[1]);
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var answer = 0L;
        for (int i = 1; i < n; ++i) {
            var delta = a[i - 1] + a[i] - x;
            if (0 < delta) {
                answer += delta;
                a[i] -= delta;
            }
            if (a[i] < 0) {
                a[i - 1] += a[i];
                a[i] = 0;
            }
        }
        Console.WriteLine(answer);
    }
}
