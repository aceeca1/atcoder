using System;

class C {
    public static void Main() {
        var s = Console.ReadLine();
        var answer = 0;
        var current = 'A';
        foreach (var i in s) {
            if (current != i) ++answer;
            current = i;
        }
        Console.WriteLine(answer - 1);
    }
}
