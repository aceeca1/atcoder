using System;

class D {
    public static void Main() {
        Console.ReadLine();
        var a = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        var minSeen = int.MaxValue >> 1;
        var maxProfit = 0;
        var argmaxSize = 0;
        foreach (var i in a) {
            var profit = i - minSeen;
            if (maxProfit < profit) {
                maxProfit = profit;
                argmaxSize = 1;
            } else if (maxProfit == profit) ++argmaxSize;
            if (i < minSeen) minSeen = i;
        }
        Console.WriteLine(argmaxSize);
    }
}
