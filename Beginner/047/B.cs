using System;

class B {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var w = int.Parse(line[0]);
        var h = int.Parse(line[1]);
        var n = int.Parse(line[2]);
        int a1 = 0, a2 = w, a3 = 0, a4 = h;
        for (int i = 0; i < n; ++i) {
            line = Console.ReadLine().Split();
            var x = int.Parse(line[0]);
            var y = int.Parse(line[1]);
            var a = int.Parse(line[2]);
            switch (a) {
                case 1: a1 = Math.Max(a1, x); break;
                case 2: a2 = Math.Min(a2, x); break;
                case 3: a3 = Math.Max(a3, y); break;
                case 4: a4 = Math.Min(a4, y); break;
            }
        }
        var deltaX = Math.Max(0, a2 - a1);
        var deltaY = Math.Max(0, a4 - a3);
        Console.WriteLine(deltaX * deltaY);
    }
}
